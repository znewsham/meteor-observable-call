import { Random } from 'meteor/random';
import { EJSON } from 'meteor/ejson';

const callbacks = {

};
function observableCallMessageHook(str) {
  if (str.indexOf("__observableCalls") === -1) {
    return;
  }
  const data = EJSON.parse(str);
  if (data.msg === "changed" && data.collection === "__observableCalls" && callbacks[data.id]) {
    callbacks[data.id](undefined, data.fields.status, false);
  }
  else if (data.msg === "removed" && data.collection === "__observableCalls") {
    delete callbacks[data.id];
  }
}
Meteor.connection._stream.on("message", observableCallMessageHook);
Meteor.observableCall = function(){
  var args = Array.prototype.slice.call(arguments, 0);
  if(!args.length || !(args[args.length - 1] instanceof Function)){
    throw new Meteor.Error("You must pass in a callback function of the form function(err, res, complete = true)");
  }
  var callId = Random.id();
  args.splice(0, 0, callId);
  var callback = args[args.length - 1];
  Meteor.call("__observableCall", ...args);
  callbacks[callId] = callback;
};
