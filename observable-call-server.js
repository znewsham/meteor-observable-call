import _ from "underscore";
const ObservableCalls = new Mongo.Collection(null);
import Future from 'fibers/future';
import { EJSON } from 'meteor/ejson';

Meteor.methods({
  async __observableCall() {
    var args = Array.prototype.slice.call(arguments, 0);
    this.__observableCallId = args[0];
    this.__observableCallName = args[1];
    this.updateCallStatus = Meteor.bindEnvironment((status, options) => {
      const invocation = DDP._CurrentMethodInvocation.get();
      if (invocation) {
        const session = invocation._session || invocation.connection;
        if (session) {
          const actualSession = Meteor.default_server.sessions.get ? Meteor.default_server.sessions.get(session.id) : Meteor.default_server.sessions[session.id];
          if (actualSession) {
            if (options && options.sendDirect) {
              const changed = {
                msg: "changed",
                collection: "__observableCalls",
                id: this.__observableCallId,
                fields: { status }
              };
              actualSession.socket.send(EJSON.stringify(changed));
            }
            else {
              actualSession.sendChanged("__observableCalls", this.__observableCallId, {status});
            }
          }
        }
      }
    });
    args.splice(0, 2);
    this.unblock();
    try {
      let result = Meteor.default_server.method_handlers[this.__observableCallName].apply(this, args);
      if (result instanceof Promise) {
        return await result;
      }
      return result;
    }
    catch (e) {
      if (e.stack && e.stack.stack) {
        e.stack = e.stack.stack;
      }
      throw e;
    }
    finally {
      const invocation = DDP._CurrentMethodInvocation.get();
      if (invocation) {
        const session = invocation._session || invocation.connection;
        if (session) {
          const actualSession = Meteor.default_server.sessions.get ? Meteor.default_server.sessions.get(session.id) : Meteor.default_server.sessions[session.id];
          if (actualSession) {
            actualSession.sendRemoved("__observableCalls", this.__observableCallId);
          }
        }
      }
    }
  }
});

Meteor.publish("__observableCalls", function(callId){

  ObservableCalls.find({ _id: callId }).observeChanges({
    added: (id, call) => {
      this.added("__observableCalls", id, call);
    },
    changed: (id, fields) => {
      this.changed("__observableCalls", id, fields);
    },
    removed: (id) => {
      this.removed("__observableCalls", id);
    }
  });
});
