#Purpose
Exposes the client side `Meteor.observableCall`
Observe the progress of long-running `Meteor.call`'s using self-defined statuses.
exposes `this.updateCallStatus` in methods made via `Meteor.observableCall`

##Usage
```javascript
//server.js
import Future from 'fibers/future';
Meteor.methods({
    myLongMethod(count){
      var i = 0;
      const future = new Future();
      const interval = setInterval(Meteor.bindEnvironment(()=>{
        this.updateCallStatus({status: {count: ++i}});
        if(i == count + 1){
          clearInterval(interval);
          future.return("finished");
        }
      }), 1000);
      return future.wait();
    }
});
```

```javascript
//client.js
Meteor.observableCall("myLongMethod", 5, function(err, status, complete = true){
  console.log(status, complete);
});

//will output:
//{count: 1}, false
//{count: 2}, false
//{count: 3}, false
//{count: 4}, false
//{count: 5}, false
//finished true
```
##NOTE: The package no longer support the status update with dollar prefix i.e ~~`this.updateCallStatus({$set: {"status.whatever": "anything"}})`~~