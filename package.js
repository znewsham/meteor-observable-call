Package.describe({
  name: 'znewsham:observable-call',
  version: '0.1.5',
  // Brief, one-line summary of the package.
  summary: 'Observable Meteor.call\'s',
  description: "Provides Meteor.observableCall - a wrapper for Meteor.call where the callback receives progress notifications.",
  // URL to the Git repository containing the source code for this package.
  git: 'https://bitbucket.org/znewsham/meteor-observable-call',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.5');
  api.use('ecmascript');
  api.use('mongo@1.1.14');
  api.mainModule('observable-call-server.js', 'server');
  api.mainModule('observable-call-client.js', 'client');
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('znewsham:observable-call');
  api.mainModule('observable-call-tests.js');
});
