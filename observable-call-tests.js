// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by observable-call.js.
import { name as packageName } from "meteor/znewsham:observable-call";

// Write your tests here!
// Here is an example.
Tinytest.add('observable-call - example', function (test) {
  test.equal(packageName, "observable-call");
});
